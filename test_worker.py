# -*- coding: utf-8 -*-

# Company: ThinkPower
# Author: Block Chen
# Date: 2020/01/13
# Mail: block.chen@thinkpower-info.com
# Support: Python 3.5

import requests
import json
import time
import random
import sys


if __name__=="__main__":
    
    print("Checking worker is alive...")
    try:
        resp = requests.get("http://127.0.0.1:7149/worker/get/alive")
    except Exception as e:
        print("Failed to connect to worker: %s" % str(e))
        sys.exit(1)
    print("Response: %s, %s" % (resp.status_code, resp.text))
    
    if resp.status_code != 200:
        print("Failed to connect to worker: %s, %s" % (resp.status_code, resp.text))
        sys.exit(1)
        
    print("You can view all data on chain from this URL: http://127.0.0.1:7149/worker/get/webpage/onchaindata")
        
    payload1 = {
        "SIGNER": "ThinkPower",
        "SECRET": "CogitoErgoSum"
    }
    
    print("Iinitiating the blockchain...")
    resp = requests.post("http://127.0.0.1:7149/worker/post/genesis", data=payload1)
    print("Response: %s, %s" % (resp.status_code, resp.text))
    
    time.sleep(3)
    
    try:
        while True:
            payload2 = {
                "SIGNER": payload1["SIGNER"],
                "SECRET": payload1["SECRET"],
                "DATAINDEX": random.randint(0, 99999),
                "SOURCE": payload1["SIGNER"],
                "MESSAGE": "test worker-%s" % random.randint(0, 999),
                "DISTINATION": payload1["SIGNER"],
                "TIMESTAMP": time.time()
            }
            
            print("Post new data to the blockchain...")
            resp = requests.post("http://127.0.0.1:7149/worker/post/data", data=payload2)
            print("Response: %s, %s" % (resp.status_code, resp.text))
            time.sleep(3)
    except KeyboardInterrupt:
        print("You can view all data on chain from this URL: http://127.0.0.1:7149/worker/get/webpage/onchaindata")
        sys.exit(0)