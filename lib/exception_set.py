# -*- coding: utf-8 -*-

# Company: ThinkPower-info
# Author: Block Chen
# Date: 2020/01/13
# Mail: block.chen@thinkpower-info.com
# Support: Python 3.5


__VERSION__="0.0.1"

class SignedError(Exception):
    pass
    
    
class SignatureError(Exception):
    pass
    
class ProofValidationError(Exception):
    pass