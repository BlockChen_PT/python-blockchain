# -*- coding: utf-8 -*-

# Company: ThinkPower
# Author: Block Chen
# Date: 2020/01/13
# Mail: block.chen@thinkpower-info.com
# Support: Python 3.5

import json
import hashlib
from . import utils
from . import exception_set


__VERSION__="0.0.1"

class DF_JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, DataFrame):
            return obj.data
        if isinstance(obj, bytearray):
            return obj.decode('utf-8')
        if hasattr(obj, 'next'):
            return list(obj)
        return json.JSONEncoder.default(self, obj)

class DataFrame():
    def __init__(self, **kwargs):
        # kwargs for one transaction record: DATAINDEX, SOURCE, MESSAGE, DISTINATION, TIMESTAMP
        self.__kwargs = {}
        self.__data = kwargs
        self.__signature = None
        self.__state = 0
        self.__signer = None
        self.__kwargs["SIGNATURE"] = self.__signature
        self.__kwargs["STATE"] = self.__state
        self.__kwargs["SIGNER"] = self.__signer
        self.__kwargs.update(kwargs)
        
    @property
    def state(self):
        if self.__state==1:
            return "SIGNED"
        return "UNSIGNED"
        
    @property
    def state_code(self):
        return self.__state
        
    @property
    def signature(self):
        return self.__signature
        
    @property
    def signer(self):
        return self.__signer
        
    @property
    def dataindex(self):
        return self.__data["DATAINDEX"]
        
    @property
    def source(self):
        return self.__data["SOURCE"]
        
    @property
    def message(self):
        return self.__data["MESSAGE"]
        
    @property
    def distination(self):
        return self.__data["DISTINATION"]
        
    @property
    def timestamp(self):
        return self.__data["TIMESTAMP"]
              
    def __str__(self):
        return json.dumps(self.__kwargs)
    
    @property
    def data(self):
        return self.__data
        
    def sign_on_data(self, signer, secret=None):
        if self.__state==0:
            self.__signer = signer
            self.__signature = utils.gen_signature(json.dumps(self.data), secret=secret)
            self.__state = 1
            self.__kwargs["SIGNATURE"] = self.__signature
            self.__kwargs["STATE"] = self.__state
            self.__kwargs["SIGNER"] = self.__signer
            
if __name__=="__main__":
    import time
    
    df = DataFrame(DATAINDEX=1,SOURCE="aaa",MESSAGE="hello world",DISTINATION="bbb",TIMESTAMP=time.time())
    print("Unsigned data: %s" % df)
    
    print("")
    secret1 = b'mysecret'
    signer1 = 'The-one'
    print("Signer: %s signed the data with secret: %s" % (signer1, secret1))
    df.sign_data(signer1, secret1)
    print("Signed data: %s" % df)
    