# -*- coding: utf-8 -*-

# Company: ThinkPower
# Author: Block Chen
# Date: 2020/01/13
# Mail: block.chen@thinkpower-info.com
# Support: Python 3.5

from . import dataframe
from . import utils
import json


__VERSION__="0.0.1"

class Block():
    def __init__(self, df_list, index, previous_hash):
        self.__signer = None
        self.__mysecret = None
        self.__previous_hash = previous_hash
        self.__signature = None
        self.__state = 0
        self.__index = index
        if all([isinstance(df, dataframe.DataFrame) for df in df_list]):            
            self.__df_list = df_list
        else:
            raise TypeError("Unexpected dataframe type was found: %s" % type(dataframe))
        
    def sign_on_block(self, signer, secret):
        self.__signer = signer
        self.__mysecret = bytearray(secret, 'utf-8')
        self.__signature =  utils.gen_signature(
            json.dumps(
                self.__df_list, 
                cls=dataframe.DF_JSONEncoder, 
                sort_keys=True
            ), 
            self.__mysecret
        )
        self.__state = 1
        return self.__signature
        
    @property
    def signature(self):
        return self.__signature
        
    @property
    def state(self):
        if self.__state==1:
            return "SIGNED"
        return "UNSIGNED"
        
    @property
    def state_code(self):
        return self.__state
        
    @property
    def data(self):
        for df in self.__df_list:
            yield df.data
        
    @property
    def df_list(self):
        return self.__df_list
        
    @property
    def index(self):
        return self.__index
        
    @property
    def dataindex_list(self):
        for df in self.__df_list:
            yield df.dataindex
        
    @property
    def previous_hash(self):
        return self.__previous_hash
        
    @property
    def signer(self):
        return self.__signer
        
    def __str__(self):
        return json.dumps(self.__dict__, cls=dataframe.DF_JSONEncoder, sort_keys=True)
        