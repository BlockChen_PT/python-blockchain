# -*- coding: utf-8 -*-

# Company: ThinkPower
# Author: Block Chen
# Date: 2020/01/13
# Mail: block.chen@thinkpower-info.com
# Support: Python 3.5

import hashlib
import hmac
import time
from datetime import datetime
import json
import os
from . import logger as _logger


__VERSION__="0.0.1"

def gen_signature(data_string, secret=None):
    if secret:
        return hmac.new(secret, msg=data_string.encode("utf-8"), digestmod=hashlib.sha256).hexdigest()
    return hashlib.sha256(data_string.encode("utf-8")).hexdigest()
    
    
def load_signer_info():
    module_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    signer_info_path = os.path.join(module_path,'config/signer.json')
    #print("Load signer info from path: %s" % signer_info_path)
    with open(signer_info_path, 'rb') as f:
        signer_info = json.load(f)
    return signer_info
    
    
GENESIS_PREVIOUS_HASH = '0' * 64
GENESIS_BLOCK_INDEX = 0
    
def load_genesis_dataframe():
    from . import dataframe
    geneis_df1 = dataframe.DataFrame(
        DATAINDEX=0,
        SOURCE=None,
        MESSAGE="Light",
        DISTINATION=None,
        TIMESTAMP=time.time()
    )
    geneis_df2 = dataframe.DataFrame(
        DATAINDEX=1,
        SOURCE=None,
        MESSAGE="of",
        DISTINATION=None,
        TIMESTAMP=time.time()
    )
    geneis_df3 = dataframe.DataFrame(
        DATAINDEX=2,
        SOURCE=None,
        MESSAGE="GENESIS",
        DISTINATION=None,
        TIMESTAMP=time.time()
    )
    return (geneis_df1,geneis_df2,geneis_df3), GENESIS_BLOCK_INDEX, GENESIS_PREVIOUS_HASH
    
    
def logging(logger=None, msg=None, level=0):
    level_info = {
        _logger.DEBUG: 'DEBUG',
        _logger.INFO: 'INFO',
        _logger.WARN: 'WARN',
        _logger.ERROR: 'ERROR',
        _logger.CRITICAL: 'CRITICAL'
    }
    
    template = "%s %s %s %s"
    
    if logger:
        if logger.level<=level:
            print(template % (datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"), logger.name, level_info[level], msg))
        if level==0:
            return logger.debug(msg)
        elif level==1:
            return logger.info(msg)
        elif level==2:
            return logger.warn(msg)
        elif level==3:
            return logger.error(msg)
        elif level==4:
            return logger.critical(msg)
    else:
        print(template % (datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"), "LOGGER", level_info[level], msg))
            
            
if __name__=="__main__":
    path = dirname(dirname(abspath(__file__)))
    print(path)
    
    