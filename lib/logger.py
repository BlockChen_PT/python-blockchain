# -*- coding: utf-8 -*-

# Company: ThinkPower-info
# Author: Block Chen
# Date: 2020/01/13
# Mail: block.chen@thinkpower-info.com
# Support: Python 3.5

import logging
import os


__VERSION__="0.0.1"


DEBUG = logging.DEBUG
INFO = logging.INFO
WARN = logging.WARNING
ERROR = logging.ERROR
CRITICAL = logging.CRITICAL


class Logger():
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARN = logging.WARNING
    ERROR = logging.ERROR
    CRITICAL = logging.CRITICAL
    
    def __init__(self, service_name, logfile_dir='.', level=logging.DEBUG):
        self.logfile = os.path.join(logfile_dir, "%s.log" % service_name)
        logging.basicConfig(
            format='%(asctime)s %(name)s %(levelname)s: %(message)s', 
            datefmt='%Y-%m-%d %H:%M:%S.%f',
            filename=self.logfile, 
            level=level
        )
        self.name = service_name
        self.level = level
        self._logger = logging.getLogger(service_name)
        
    def debug(self, msg):
        self._logger.debug(msg)
        
    def info(self, msg):
        self._logger.info(msg)
        
    def warn(self, msg):
        self._logger.warning(msg)
        
    def error(self, msg):
        self._logger.error(msg)
        
    def critical(self, msg):
        self._logger.critical(msg)
        