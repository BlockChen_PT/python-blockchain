# -*- coding: utf-8 -*-

# Company: ThinkPower
# Author: Block Chen
# Date: 2020/01/13
# Mail: block.chen@thinkpower-info.com
# Support: Python 3.5

import hashlib
import json
from . import utils
from . import logger as _logger


__VERSION__="0.0.1"

class POW():
    def __init__(self, difficulty=2, logger=None):
        self.__difficulty = difficulty
        self.name = "Proof-of-Work"
        self.logger=logger
    
    @property
    def difficulty(self):
        return self.__difficulty
    
    def proof(self, block):
        block.nonce = 0
        hash_value = hashlib.sha256(str(block).encode('utf-8')).hexdigest()
        
        while not hash_value.startswith('0' * self.__difficulty):
            block.nonce += 1
            hash_value = hashlib.sha256(str(block).encode('utf-8')).hexdigest()
            utils.logging(logger=self.logger, msg="POW nonce: %s, hash: %s" % (block.nonce, hash_value), level=_logger.DEBUG)
        return block, hash_value 
        
    def validate_proof(self, block, hash_value):
        condition1 = hash_value.startswith('0' * self.__difficulty)
        condition2 = hashlib.sha256(str(block).encode('utf-8')).hexdigest()==hash_value
        return condition1 and condition2