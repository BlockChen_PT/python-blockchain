# -*- coding: utf-8 -*-

# Company: ThinkPower
# Author: Block Chen
# Date: 2020/01/13
# Mail: block.chen@thinkpower-info.com
# Support: Python 3.5


from . import utils
from . import block as _block
from . import proof_sets
from . import exception_set
from . import dataframe
from traceback import format_exc
import json
from . import logger as _logger


__VERSION__="0.0.1"

class BlockChain():
    # update_unproven_chain=>work=>proof=>validate_proof=>update_chain
    def __init__(self, logger=None, **kwargs):
        self.logger = logger
        self.__unproven_df_list = [] #[df0, df1,...]
        self.__chain = [] # [(block0, hash0), (block1, hash1),...]
        self.difficulty = 2
        self.name = "DemoChain"
        self.__had_genesis = 0
        
        _args = kwargs.keys()
        if "DIFFICULTY" in _args:
            self.difficulty = kwargs["DIFFICULTY"]
        if "NAME" in _args:
            self.name = kwargs["NAME"]
        
        self.__proof_method = proof_sets.POW(difficulty=self.difficulty, logger=self.logger)
        
        utils.logging(logger=self.logger, 
                msg="Initated BlockChain: %s, proof methods: %s, default_difficulty: %s" % (
                    self.name,
                    self.__proof_method.name,
                    self.difficulty
                    ), 
                level=_logger.INFO
            )
        
    def __str__(self):
        return str(self.__chain)
        
    def create_genesis_block(self):
        if self.__had_genesis == 0:
            GENESIS = _block.Block(*utils.load_genesis_dataframe())
            signer_info = utils.load_signer_info()
            GENESIS.sign_on_block(signer_info["SIGNER"], signer_info["SECRET"])
            utils.logging(logger=self.logger, msg="GENESIS signature: %s, state: %s" % (GENESIS.signature, GENESIS.state), level=_logger.DEBUG)
            self.__unproven_df_list = [] # clear it
            self.__chain = [] # clear it
            proofed_block, hash_value = self.proof(GENESIS, signer_info["SIGNER"], signer_info["SECRET"])
            self.update_chain(proofed_block, hash_value)
            self.__had_genesis = 1
            return proofed_block, hash_value
        return (None, None)
        
    def update_chain(self, block, hash_value):
        utils.logging(logger=self.logger, 
                msg="Proofing block, index: %s, state: %s, sign: %s, hash: %s" % (
                    block.index, block.state_code, block.signature, hash_value
                    ), 
                level=_logger.DEBUG
            )
        if isinstance(block, _block.Block):
            if self.validate_proof(block, hash_value):
                self.__chain.append((block, hash_value))
                if block.state_code==1:
                    utils.logging(logger=self.logger, 
                        msg="Success to add block, index: %s, state: %s, sign: %s, hash: %s" % (
                            block.index, block.state_code, block.signature, hash_value
                            ), 
                        level=_logger.INFO
                    )
                else:
                    utils.logging(logger=self.logger, 
                        msg="Success to add UNSIGNED block index: %s, state: %s, sign: %s, hash: %s" % (
                            block.index, block.state_code, block.signature, hash_value
                            ), 
                        level=_logger.WARN
                    )
            else:
                raise _exceptions.ProofValidationError("Proof validation Failed, block: %s" % (block))
        else:
            raise TypeError("Unexcepted block type was found: %s" % type(block))
    
    @property
    def last_block(self):
        # return (block, hash)
        try:
            return self.__chain[-1]
        except IndexError:
            return (None , None)
        
    @property
    def chain(self):
        return self.__chain
        
    @property
    def unproven_df_list(self):
        return self.__unproven_df_list
        
    def proof(self, block, signer=None, secret=None):
        _block, _hash_value = self.__proof_method.proof(block)
        if block.state_code==1:
            sign = block.sign_on_block(signer, secret)
            _sign = _block.sign_on_block(signer, secret)
            if not (sign==_sign):
                raise exception_set.SignatureError("Data signature %s dosen't match %s, _data: %s, data: %s" % (_sign, sign, _block.data, block.data))
        return _block, _hash_value
            
    def validate_proof(self, block, hash_value):
        return self.__proof_method.validate_proof(block, hash_value)
        
    def update_unproven_list(self, df):
        if isinstance(df, dataframe.DataFrame):
            self.__unproven_df_list.append(df)
        else:
            raise TypeError("Unexcepted dataframe type was found: %s" % type(df))
            
    def create_block(self, signer=None, secret=None):
        if len(self.__unproven_df_list)<1:
            # no new data, no new block
            return (None, None)
        else:
            try:
                last_block, last_hash = self.last_block
                new_block = _block.Block(
                self.__unproven_df_list,
                last_block.index +1,
                last_hash
                )
                # if signer and secret was passed, sign on the new block with the signer
                if signer and secret:
                    new_block.sign_on_block(signer, secret)
                
                proofed_block, hash = self.proof(new_block, signer, secret)
                self.update_chain(proofed_block, hash)
                self.__unproven_df_list = []
                return proofed_block, hash
            except Exception as e:
                utils.logging(logger=None, 
                    msg="Failed to create block: %s" % (
                        str(e),
                        ), 
                    level=_logger.ERROR
                )
                utils.logging(logger=None, 
                    msg="Traceback: %s" % (
                        format_exc(),
                        ), 
                    level=_logger.INFO
                )
                return (None, None)
                
    def dump(self):
        # dump all data from DemoChain into json format
        # an iterator will be returned
        for block, hash_value in self.chain:
            resp_tmp = {}
            datalist = []
            resp_tmp['INDEX'] = block.index 
            resp_tmp['HASH'] = hash_value
            resp_tmp['PREVIOUSHASH'] = block.previous_hash
            resp_tmp['SIGNER'] = block.signer
            resp_tmp['SIGNATURE'] = block.signature    
            for ele in  block.data:
                datalist.append(ele)
            resp_tmp['DATA'] = datalist
            yield resp_tmp
                
    def streaming_dump(self):
        _switch = True
        processed_index = 0
        utils.logging(logger=self.logger, 
            msg="Start streaming transfer onchaindata", 
            level=_logger.INFO
         )
        try:
            while _switch:
                if len(self.chain)>processed_index:
                    _chain = self.chain[processed_index:]
                    for block, hash_value in _chain:
                        resp_tmp = {}
                        datalist = []
                        resp_tmp['INDEX'] = block.index 
                        resp_tmp['HASH'] = hash_value
                        resp_tmp['PREVIOUSHASH'] = block.previous_hash
                        resp_tmp['SIGNER'] = block.signer
                        resp_tmp['SIGNATURE'] = block.signature    
                        for ele in  block.data:
                            datalist.append(ele)
                        resp_tmp['DATA'] = datalist
                        yield resp_tmp
                        processed_index = block.index
                else:
                    time.sleep(1)
        except Exception as e:
            _switch = False
        utils.logging(logger=self.logger, 
            msg="Stop streaming transfer onchaindata", 
            level=_logger.INFO
         )