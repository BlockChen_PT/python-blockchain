# -*- coding: utf-8 -*-

# Company: ThinkPower-info
# Author: Block Chen
# Date: 2020/01/13
# Mail: block.chen@thinkpower-info.com
# Support: Python 3.5

from flask import Flask, request, abort, Response, stream_with_context, render_template
from lib.blockchain import BlockChain
from lib import utils, dataframe
from lib import logger
import json
import re
import time
#from multiprocessing import Process


__VERSION__="0.0.1"

service_name = "DemoChain"
logfile_dir = "."
_logger = logger.Logger(service_name, logfile_dir, level=logger.INFO) # If debug mode is needed, set level=logger.DEBUG
signer_info = utils.load_signer_info() #{"SIGNER": xxx,"SECRET": xxx}

INVALID_SIGNER_RESPONSE = "Invalid Signer. Please check your signer name or secret."

app =  Flask(__name__)
BC = BlockChain(logger=_logger, NAME=service_name)

        
def validate_user(request):
    signer = request.values["SIGNER"]
    secret = request.values["SECRET"]
    if (signer==signer_info["SIGNER"]) and (secret==signer_info["SECRET"]):
        return True
    return False


### APIs

# initiate DemoChain on the worker
@app.route('/worker/post/genesis', methods=['POST'])
def gensis():
    if request.method=="POST":
        if validate_user(request):
            pass
        else:
            print(INVALID_SIGNER_RESPONSE)
            utils.logging(logger, INVALID_SIGNER_RESPONSE, logger.ERROR)
            abort(400)
    #prcs = Process(target=BC.create_genesis_block)
    #prcs.start()
    BC.create_genesis_block()
    return "OK"
    
@app.route('/worker/get/onchaindata', methods=['GET'])
def get_onchaindata():
    def gen():
        for onchaindata in BC.dump():
            yield json.dumps(onchaindata) + "\r\n"
    return Response(gen(), mimetype="application/json")
    
@app.route('/worker/get/streaming-onchaindata', methods=['GET'])
def get_streaming_onchaindata():
    def gen_streamer():
        streamer = BC.streaming_dump()
        _switch = True
        for onchaindata in streamer:
            yield json.dumps(onchaindata) + "\r\n"
            #yield ('--onchaindata\r\nContent-Type:application/json\r\n\r\n' + json.dumps(onchaindata) + '\r\n')
    return Response(gen_streamer(), mimetype="text/plain")
    #return Response(gen_streamer(), mimetype='multipart/x-mixed-replace; boundary=onchaindata')
    
@app.route('/worker/get/alive', methods=['GET'])
def alive():        
    return "OK"

@app.route('/worker/post/data', methods=['POST'])
def work():
    # equal to mine function in bitcoin chain
    if validate_user(request):
        # dataframe kwargs: DATAINDEX, SOURCE, MESSAGE, DISTINATION, TIMESTAMP
        df = dataframe.DataFrame(
            DATAINDEX=request.values["DATAINDEX"],
            SOURCE=request.values["SOURCE"],
            MESSAGE=request.values["MESSAGE"],
            DISTINATION=request.values["DISTINATION"],
            TIMESTAMP=request.values["TIMESTAMP"]
        )
        BC.update_unproven_list(df)
        BC.create_block(signer=request.values["SIGNER"], secret=request.values["SECRET"])
        #prcs = Process(target=BC.create_block)
        #prcs.start()
        return "OK"
    else:
        abort(400)


### webpages

@app.route('/worker/get/webpage/onchaindata', methods=['GET'])
def get_webpage_onchaindata():
    return render_template("onchaindata.html")
    
if __name__=="__main__":
    #app.debug = True
    app.run(host='127.0.0.1', port=7149)
    