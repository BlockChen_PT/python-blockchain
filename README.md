# 單機版區塊鍊工作節點程式碼範例

## 依賴
1. PYTHON 3.5 以上
2. 運行worker: FLASK 1.1.1 以上 (pip install flask)

## 參考資料
1. [昕力資訊技術小聚｜Blockchain 區塊鏈快閃！你不可不知的區塊鏈原理與實際應用](https://www.youtube.com/watch?v=lBj55Gn2DGI)
2. [使用Python從零開始開發區塊鍊應用程序](https://www.ibm.com/developerworks/cn/cloud/library/cl-develop-blockchain-app-in-python/index.html)
3. 區塊鍊簡介: documents/blockchain_introduction.pptx

## 運行
### worker單節點工作(持續挖礦)
1. 打開一個終端介面,運行python worker.py, 這會在localhost的port 7149建立一個worker服務(Ctrl+c終止運行)
2. proof過程的log被定義為DEBUG等級, 若需要打開debug模式, 於worker.py宣告logger.Logger物件處(第21行),將level設置為DEBUG等級
3. 打開另一個終端介面運行test_worker.py, 它會測試worker服務是否存在,然後發送API給worker初始化區塊鍊,以及持續的發送資料(間隔3秒)給worker進行打包成區塊等工作(Ctrl+c終止運行)
4. 於運行的目錄下會生成DemoChain.log文件
5. 從網頁查看最新的區塊資訊以及所有區塊的資料 http://127.0.0.1:7149/worker/get/webpage/onchaindata
![avatar](imgs/streaming_onchaindata.jpg)

## worker API說明
1. http://127.0.0.1:7149/worker/get/alive, 方法:GET, 測試worker服務存活,回應200,內文為OK
2. http://127.0.0.1:7149/worker/post/genesis, 方法POST, 送出worker擁有者的signer資訊請求初始化區塊鍊,回應200,內文為OK;若signer驗證失敗回應400,內文為"Invalid Signer. Please check your signer name or secret."; 注意重複發送此API,對於已被初始化的區塊鍊不會被再次初始化(仍回應200,OK);若有此需求請直接重啟worker服務
3. http://127.0.0.1:7149/worker/post/data, 方法POST, 請求worker將post的資料加入區塊鍊, 資料格式為JSON,需有keys: SIGNER,SECRET,DATAINDEX,SOURCE,MESSAGE,DISTINATION, TIMESTAMP; 回應200,OK;signer驗證失敗回應同worker/genesis
4. http://127.0.0.1:7149/worker/get/streaming-onchaindata, 方法GET, 利用資料串流持續更新區塊鍊資料
5. http://127.0.0.1:7149/worker/get/onchaindata, 方法GET, 請求worker倒出區塊鍊上的所有訊息, 回應200, 內文為多筆JSON格式的字串,如下圖
![avatar](imgs/onchaindata.jpg)

## worker工作說明
### 初始化
1. worker啟動後建立web服務, 打開port 7149聆聽API請求, 並調用BlockChain類別生成區塊鍊物件
2. 接收到genesis API請求後, worker調用`blockchain.create_genesis_block`方法建造創世(初始)區塊,並進行驗證與加入區塊鍊
### 工作(挖礦)
1. worker接收到新資料後調用work方法
2. work方法內調用區塊鍊物件的`proof`方法,找出滿足POW(proof of work)困難度條件的hash值(預設條件為區塊的hash值開頭是"00")
3. 將找到的hash值與區塊傳入`validate_proof`方法進行驗證(驗算)
4. 將通過驗證的區塊以(block, hash)的tuple格式加到區塊鍊上, 到此完成一遍work方法

## 單元測試
### 測試blockchain模組
1. 運行python test_blockchain.py
2. 此測試在成功對創世區塊(`GENESIS` block)挖到礦(完成區塊驗證),由區塊鍊倒出區塊資料後即結束運行
![avatar](imgs/test_blockchain.jpg)
3. proof過程的log被定義為DEBUG等級, 若需要打開debug模式, 於test_blockchain.py宣告logger.Logger物件處(第19行),將level由INFO設置為DEBUG等級
![avatar](imgs/blockchain-demo.jpg)

### 測試block模組
1. 運行test_block.py
2. 此測試包含調用DataFrame產生兩筆假資料,調用Block模組將兩筆假資料生成區塊,以及簽署區塊的功能

### 測試worker模組
1. 打開一個終端介面,運行python worker.py, 這會在localhost的port 7149建立一個worker服務(Ctrl+c終止運行)
2. 若需要打開debug模式, 於worker.py宣告logger.Logger物件處(第21行),將level設置為DEBUG等級
3. 打開另一個終端介面運行test_worker.py, 它會測試worker服務是否存在,然後發送API給worker初始化區塊鍊,以及持續的發送資料(間隔3秒)給worker進行打包成區塊等工作(Ctrl+c終止運行)
![avatar](imgs/test_worker.jpg)

### 關於Logger可設置的等級(預設為INFO)與對應的int值
1. DEBUG: 10
2. INFO: 20
3. WARN: 30
4. ERROR: 40
5. CRITICAL: 50

## 程式文件說明
1. config/signer.json: 定義signer的資訊,本demo範例已實作signer對data簽署生成signature,用於驗證資料是否遭竄改
2. lib/block.py: 定義區塊(block)類別,一個區塊可以承載多筆資料(dataframes)
3. lib/blockchain.py: 定義區塊鍊(blockchain),由proof_set.py模組引入proof與validate_proof方法,故可由proof_set.py抽換proof算法
4. lib/dataframe.py: 定義區塊傳遞資料的格式
5. lib/exception_set.py: 定義額外的exceptions
6. lib/logger.py: 日誌生成器, 運行worker時會於運行目錄下生成log文件
7. lib/proof_set.py: proof算法模組
8. lib/utils.py: 定義簽名算法,創世區塊的初始化使用的dataframe資訊,導入signer,logger的統一入口模組
9. worker.py: 將區塊鍊封裝成web服務,接收API請求工作,預設聆聽port 7149

# 關於區塊鍊
## 名詞解釋
1. `DataFrame`: 資料框架,對應單筆資料,用於約束資料格式,Signer能對單筆資料進行驗證簽名
2. `df_list`: 資料清單,多筆資料的集合(dataframe_list),目前沒有限制list長度上限
3. `Block`: 區塊,一個`Block`對應一個`df_list`,Signer能在Block層級進行簽名;Blcok相當於完成proof validation的df_list
4. `proof`: 證明,不同算法定義不同意義的證明,如工作證明,權益證明,時間證明等;有些consensus算法與proof算法有很深的依賴性
5. `proof validation`: 驗證/檢驗證明,查核`proof`是否有造假的算法
6. `BlockChain`: 區塊鍊,Block的集合,由每一個`Block`與`前一個Block的hash值`連接成為一個類似list資料結構的類別
7. `Signer`: 簽署者,可以是一個組織或個人,根據對象指定的secret可對單筆資料(dataframe)或多筆資料(block)進行簽名與驗證
8. node/peer/`worker`: 節點,將df_list進行驗證成為Block的程序,能提供API與節點通訊,亦是智能合約(smart contract)的執行者;每個節點擁有自己的一條區塊鍊,經由共識算法達成一致性
9. `work`/mine: 節點的主要工作(main function),實際上是指產生proof與驗證proof的過程,若完成工作能獲得酬勞則稱為挖礦(mining)
10. `consensus`: 指多個node/peer/worker之間達成一致性的算法,目前已知的共識算法多達30種以上;有些共識算法與proof算法有依賴性,使得跨不同proof算法的鍊進行交易或整合變得非常困難
11. `常見迷思`: 使用相同proof算法與consensus算法並互相通訊的節點被視為在"同一條鍊上工作",這是一種似是而非的說法
12. `正確說法`: 每個節點擁有各自的BlockChain,經由通訊去互相檢查proof證明,經由consensus算法使得各自擁有的BlockChain達成一致性(達成共識),這些節點屬於同一個區塊鍊叢集

## 與資料庫的異同
1. 都是儲存資料的工具, 性質大同小異
2. 與資料庫的差別在於對於"人的信任程度"
3. 資料庫能輕易(低成本)修改資料,區塊鍊上修改資料需要不合理的過高成本,使得區塊鍊上的資料"幾乎沒人能去修改"
4. 承第三點,若資料庫管理者當中有人不可信,則資料庫的資料亦不可信;區塊鍊基於拜占庭難題設計,若節點管理者當中有少數人不可信,區塊鍊仍能維持資料可信;若以人員絕對可信為前提則沒有討論區塊鍊的必要
5. 承第三點,資料庫需要放在系統內部,施加層層保護;區塊鍊"有本錢"直接暴露於網路上,所以更適合面向大眾(公有鍊),機構施政(聯盟鍊)的應用場景
6. 資料庫叢集與區塊鍊叢集皆能滿足資料一致性的目標,資料庫使用"交易中資料上鎖"的方式維護強一致性,而區塊鍊基於共識算法,偏向"最終會一致"的弱一致性
7. 承第一點, 資料進入資料庫之前能被竄改,進入區塊鍊之前亦能被竄改;諸如此類的問題不會因為由資料庫轉移至區塊鍊而獲得解決
8. 承名詞解釋第12點,每個節點皆擁有完整的區塊鍊資訊,這使得節點之間可以互相替代,因此單節點故障並不影響區塊鍊叢集運作;資料庫為了維護強一致性,可能會犧牲高可用性,只要單節點故障就可能鎖定所有節點阻止新的交易發生

## 區塊鍊解決什麼問題(去中心化)
1. 假設有A跟B兩個機構,約定一個資料庫存放A與B之間交易帳本紀錄,每個月底根據這些紀錄進行現金支付結算
2. 問題一:資料庫該建在A機構的系統內受到層層保護?A機構的管理者做假帳的成本(或著說難度)其實很低?那麼B機構該如何查帳?經由A機構提供的API看到的帳本資料是否為假?派人進A公司查帳有法規限制(在法治國家,這是司法機關的權限,B機構需要會同司法機關人員持票據才能執行)
3. 解答一: 傳統的解決方案是增加一個C機構作為公正第三方機構,將資料庫建在C機構內(金融機構)
4. 問題二: C機構是否會跟A機構串通給予更多的"優惠"?A機構與B機構看到的交易帳本如何驗證是同一本?簡單一句話就是,`金融機構的資料庫並不是透明的,如何驗證其"公正性"?`
5. 解答二: 由政府機關,如台灣的金管會監督金融機構,金融機構"不公正的"行為在"證據齊全"的前提下會受到處罰,以此嚇阻不公正的行為,但僅能在事後處罰,很難規劃出事先驗證一個機構符合"公平,公正,公開"的科學方法
6. `區塊鍊解決方案`: 取代資料庫,A與B機構各自擁有數個節點構成一個區塊鍊叢集,將交易帳本資訊存放至區塊鍊,則A與B機構各自擁有的節點能保障擁有一致的帳本資料,且B機構只需訪問自己的節點即可查帳,依照此帳本記錄做現金結算能有最大的保障;因為無論是A或B機構,若要竄改區塊鍊上的資訊需要支付遠超過帳本身價值的成本

# 待辦:
## 尚未開發
1. 定義節點間的通訊機制(需要推播功能),考慮設計私有通訊協議
2. 定義共識(consensus)算法模組
3. 資料與通訊加密機制

## 已知bug
1. 在worker尚未初始化區塊鍊之前就收到post data請求報錯導致worker異常終止運行
2. woker輸出log的日期格式出現%f的亂碼
3. 目前worker服務為單線程,在接收/post/data後執行驗證等工作完成後才給出http response,若驗證算法需要一段時間,則有client端timeout的疑慮