# -*- coding: utf-8 -*-

# Company: ThinkPower
# Author: Block Chen
# Date: 2020/01/13
# Mail: block.chen@thinkpower-info.com
# Support: Python 3.5

__VERSION__="0.0.1"


from lib.blockchain import BlockChain
from lib import logger


if __name__=="__main__":
    name = "DemoChain"
    logfile_dir = "."
    _logger = logger.Logger(name, logfile_dir, logger.INFO)


    print("Create DemoChain")
    BC = BlockChain(logger=_logger, NAME=name)
    BC.create_genesis_block()
    
    print("")
    print("Dump data from DemoChain")
    for ele in BC.dump():
        print(ele)
