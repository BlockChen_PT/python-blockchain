# -*- coding: utf-8 -*-

# Company: ThinkPower
# Author: Block Chen
# Date: 2020/01/13
# Mail: block.chen@thinkpower-info.com
# Support: Python 3.5

from lib import dataframe
from lib import utils
from lib import block


__VERSION__="0.0.1"

if __name__=="__main__":
    import time
    
    df1 = dataframe.DataFrame(DATAINDEX=1,SOURCE="aaa",MESSAGE="hello world",DISTINATION="bbb",TIMESTAMP=time.time())
    df2 = dataframe.DataFrame(DATAINDEX=2,SOURCE="ccc",MESSAGE="hello block",DISTINATION="ddd",TIMESTAMP=time.time())
    
    df_list = [df1, df2]
    
    one_block = block.Block(df_list, 77, utils.GENESIS_PREVIOUS_HASH) # Block(df_list, index, previous_hash)
    print("Data in block:")
    for data in one_block.data:
        print(data)
        
    one_block.sign_on_block(signer="test-signer", secret="secret")
    print("Block sign: %s" % one_block.signature)